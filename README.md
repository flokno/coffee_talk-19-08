Coffee Talk on August 5
===

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/flokno%2Fcoffee_talk-19-08/master?filepath=analysis.ipynb)
[![License](https://img.shields.io/badge/license-Beer-blue.svg)](https://en.wikipedia.org/wiki/Beerware)

Author: [Florian Knoop](https://gitlab.com/flokno)

## Instructions

Find the [slides](https://docs.google.com/presentation/d/1RDm2cux2KkMBUNPq_yQEDaPGvg49opggzY7TVBGEWEg/edit?usp=sharing)

Open Notebook

```bash
jupyter notebook analysis.ipynb
```

or click on the [binder](https://mybinder.readthedocs.io/en/latest/) badge above for running the notebook remotely.

Have fun!

#### Installation

If you experience problems running the notebook locally, please consider to use a virtual environment and install the dependencies from `requirements.txt`:

```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
jupyter notebook analysis.ipynb   
```
